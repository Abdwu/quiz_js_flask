from flask import jsonify, request, render_template
from .app import app
from .models import(
    db_create_question,
    db_create_quiz,
    db_get_all_quiz,
    db_get_quiz,
    Question,
    db_get_all_question,
    db_get_question,
    db_modif_question,
    db_modif_quiz,
    db_delete_question,
    db_delete_quiz
)

@app.route('/')
def main():
    return render_template('quiz.html')

@app.route('/quiz/api/v1.0/quiz/', methods=["GET"])
def get_all_quiz():
    return jsonify(
        [q.to_json() for q in db_get_all_quiz()]
    )
    
@app.route('/quiz/api/v1.0/quiz/<int:quiz_id>', methods=["GET"])
def get_quiz(quiz_id):
    return jsonify(db_get_quiz(quiz_id).to_json())

@app.route('/quiz/api/v1.0/question/', methods=["GET"])
def get_all_question():
    return jsonify([q.to_json() for q in db_get_all_question()])

@app.route('/quiz/api/v1.0/question/<int:question_id>', methods=["GET"])
def get_question(question_id):
    return jsonify(db_get_question(question_id).to_json())

@app.route('/quiz/api/v1.0/quiz', methods=["POST"])
def create_quiz():
    json = request.json
    return jsonify(
        db_create_quiz(json).to_json()
    )
    
@app.route('/quiz/api/v1.0/question', methods=["POST"])
def create_question():
    json = request.json
    return jsonify(
        db_create_question(json).to_json()
    )

@app.route('/quiz/api/v1.0/question/<int:question_id>', methods=["PUT"])
def modif_question(question_id):
    json = request.json
    return jsonify(
        db_modif_question(question_id, json).to_json()
    )

@app.route('/quiz/api/v1.0/quiz/<int:quiz_id>', methods=["PUT"])
def modif_quiz(quiz_id):
    json = request.json
    return jsonify(
        db_modif_quiz(quiz_id, json).to_json()
    )

@app.route('/quiz/api/v1.0/question/<int:question_id>', methods=["DELETE"])
def delete_question(question_id):
    return jsonify(
        db_delete_question(question_id).to_json()
    )

@app.route('/quiz/api/v1.0/quiz/<int:quiz_id>', methods=["DELETE"])
def delete_quiz(quiz_id):
    return jsonify(
        db_delete_quiz(quiz_id).to_json()
    )