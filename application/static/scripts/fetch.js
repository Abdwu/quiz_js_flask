function get_all_quiz() {
    fetch('/quiz/api/v1.0/quiz')
        .then((rep) => {
            rep.json()
                .then((json) => console.log(json));
        }).catch((err) => console.error(err));
}

async function get_all_quiz2() {
    const rep = await fetch('/quiz/api/v1.0/quiz');
    const json = await rep.json();
    console.log(json);
    show_all_quiz(json);
}

async function get_quiz(quiz_id) {
    const rep = await fetch(`/quiz/api/v1.0/quiz/${quiz_id}`);
    const json = await rep.json();
    console.log(json); 
}

async function create_quiz(json) {
    console.log(json);
    const rep = await fetch('/quiz/api/v1.0/quiz',
        {
            "method": "POST",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": JSON.stringify(json)
        });
    json = await rep.json();
    console.log(json);
    get_all_quiz2();
}

async function create_question(json) {
    const rep = await fetch('/quiz/api/v1.0/question',
        {
            "method": "POST",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": JSON.stringify(json)
        });
    json = await rep.json();
    console.log(json);
    getQuestionsForQuiz(json['quiz_id']);
    }

async function get_all_question() {
    const rep = await fetch('/quiz/api/v1.0/question');
    const json = await rep.json();
    console.log(json);
}

async function show_all_quiz(quizs) {
    const div = $("#all-quiz");
    div.empty();

    for (const q of quizs) {
        const container = $("<div>")
            .addClass("enflex") // Ajout de la classe quiz-link
        ;
        const quizLink = $("<a>")
            .text(q["name"])
            .attr("href", "#")
            .addClass("quiz-link") // Ajout de la classe quiz-link
            .click(() => {
                // Lorsque le lien est cliqué, appeler la fonction pour afficher les questions du quiz
                getQuestionsForQuiz(q["quiz_id"]);
            });

        const lesButton = getButtonmodifsEtSupp(q["quiz_id"]);


        div.append(quizLink);
        
        const buttonGroup = $("<div>").addClass("button-group");

        
        buttonGroup.append(lesButton[0]);
        buttonGroup.append(lesButton[1]);

        div.append(buttonGroup);
        
        // div.append(container)


        

    }
    // Ajouter le formulaire pour ajouter un quiz
    div.append(createQuizForm());
}

function getButtonmodifsEtSupp(quiz_id) {

    const modifierButton = $("<button>")
        .text("Modifier")
        .addClass("delete-button") // Ajout de la classe delete-button
        .addClass("modifier-button") // Ajout de la classe delete-button
        .click(() => {
            // Lorsque le bouton "Modifier" est cliqué, appeler la fonction pour modifier la question
            modifier_quiz(quiz_id);
        });

    const deleteButton = $("<button>")
        .text("Supprimer")
        .addClass("delete-button") // Ajout de la classe delete-button
        .click(() => {
            // Lorsque le bouton "Supprimer" est cliqué, appeler la fonction pour supprimer la question
            delete_quiz(quiz_id);
        });

    return [modifierButton,deleteButton];
    
}

function modifier_une_question(question_id,id_quiz){
        // Demander à l'utilisateur le nouveau nom du quiz
        const newQestionName = window.prompt("Entrez le nouveau nom du quiz :");
        if (newQestionName !== null) {
            // Si l'utilisateur a saisi un nouveau nom, procéder à la modification du quiz
            const formData = {
                id: question_id,
                title: newQestionName,
                quizId: id_quiz
            };
            modifier_question(formData);
        }
}


function getButtonmodifsEtSupp2(question_id) {

    const modifierButton = $("<button>")
        .text("Modifier")
        .addClass("delete-button") // Ajout de la classe delete-button
        .addClass("modifier-button") // Ajout de la classe delete-button
        .click(() => {
            // Lorsque le bouton "Modifier" est cliqué, appeler la fonction pour modifier la question
            modifier_une_question(question_id);
        });

    const deleteButton = $("<button>")
        .text("Supprimer")
        .addClass("delete-button") // Ajout de la classe delete-button
        .click(() => {
            // Lorsque le bouton "Supprimer" est cliqué, appeler la fonction pour supprimer la question
            delete_question(question_id);
        });

    return [modifierButton,deleteButton];
    
}

async function modifier_quiz(quiz_id) {
    // Demander à l'utilisateur le nouveau nom du quiz
    const newQuizName = window.prompt("Entrez le nouveau nom du quiz :");
    if (newQuizName !== null) {
        // Si l'utilisateur a saisi un nouveau nom, procéder à la modification du quiz
        const formData = {
            id: quiz_id,
            name: newQuizName
        };
        modifier_un_quiz(formData);
    }
}

async function getQuestionsForQuiz(quizId) {
    // Effectuer une requête pour obtenir les questions associées à ce quiz
    const response = await fetch(`/quiz/api/v1.0/quiz/${quizId}`);
    const quizData = await response.json();

    // Afficher les questions juste en dessous du quiz sélectionné
    const questionsDiv = $("#questions-div");
    questionsDiv.empty();

    // Afficher les questions dans une liste
    const questionsList = $("<ul>").addClass("question-list"); // Ajout de la classe question-list
    for (const question of quizData.questions) {
        // Créer un élément de liste pour chaque question
        const questionItem = $("<li>").text(question.title).addClass("question-item"); // Ajout de la classe question-item
        // Créer le bouton de suppression
        const lesButtons = getButtonmodifsEtSupp2(question.id);
        // Ajouter le bouton de suppression à l'élément de liste de la question
        questionItem.append(lesButtons[0]);
        // Ajouter le bouton de modification à l'élément de liste de la question
        questionItem.append(lesButtons[1]);
        // Ajouter l'élément de liste de la question à la liste de questions
        questionsList.append(questionItem);
    }

    // Ajouter le titre et la liste de questions au conteneur des questions
    questionsDiv.append("<h2>Questions du quiz</h2>").append(questionsList);
    // Ajouter le formulaire pour ajouter une question
    questionsDiv.append(createQuestionForm(quizId));
}

function createQuizForm() {
    // Créer un formulaire pour ajouter un quiz
    const form = $("<form>").attr("id", "create-quiz-form");
    // Ajouter un champ pour le nom du quiz
    form.append($("<input>").attr("type", "text").attr("name", "name").attr
        ("placeholder", "Nom du quiz"));
    // Ajouter un bouton pour soumettre le formulaire
    form.append($("<input>").attr("type", "submit").val("Créer"));
    // Ajouter un gestionnaire d'événements pour le formulaire
    form.submit((event) => {
        // Empêcher le formulaire de se soumettre normalement
        event.preventDefault();
        // Récupérer les données du formulaire
        const formData = form.serializeArray();
        // Créer un objet à partir des données du formulaire
        const json = {};
        for (const field of formData) {
            json[field.name] = field.value;
        }
        // Appeler la fonction pour créer un quiz
        create_quiz(json);
        get_all_quiz2();
    });
    return form;
}

function createQuestionForm(quizId) {
    // Créer un formulaire pour ajouter une question
    const form = $("<form>").attr("id", "create-question-form");
    // Ajouter un champ pour le titre de la question
    form.append($("<input>").attr("type", "text").attr("name", "title").attr
        ("placeholder", "Titre de la question"));
    // Ajouter un champ pour le quiz_id
    form.append($("<input>").attr("type", "hidden").attr("name", "quiz_id").val(quizId));
    // Ajouter un bouton pour soumettre le formulaire
    form.append($("<input>").attr("type", "submit").val("Créer"));
    // Ajouter un gestionnaire d'événements pour le formulaire
    form.submit((event) => {
        // Empêcher le formulaire de se soumettre normalement
        event.preventDefault();
        // Récupérer les données du formulaire
        const formData = form.serializeArray();
        // Créer un objet à partir des données du formulaire
        const json = {};
        for (const field of formData) {
            json[field.name] = field.value;
        }
        // Appeler la fonction pour créer une question
        create_question(json);
        // getQuestionsForQuiz(quizId);
    });
    return form;
}


async function modifier_un_quiz(json) {
    const rep = await fetch(`/quiz/api/v1.0/quiz/${json["id"]}`,
        {
            "method": "PUT",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": JSON.stringify(json)
        });
    json = await rep.json();
    console.log(json);
    get_all_quiz2();
}

async function modifier_question(json) {
    const rep = await fetch(`/quiz/api/v1.0/question/${json["id"]}`,
        {
            "method": "PUT",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": JSON.stringify(json)
        });
    json = await rep.json();
    console.log(json);
    getQuestionsForQuiz(json.quiz_id);
}

async function delete_quiz(quiz_id) {
    const rep = await fetch(`/quiz/api/v1.0/quiz/${quiz_id}`,
        {
            "method": "DELETE"
        });
    json = await rep.json();
    console.log(json);
    get_all_quiz2();
}

async function delete_question(question_id) {
    const rep = await fetch(`/quiz/api/v1.0/question/${question_id}`,
        {
            "method": "DELETE"
        });
    json = await rep.json();
    console.log(json);
    // Recharger les questions du quiz
    getQuestionsForQuiz(json.quiz_id);
}

get_all_quiz2();
