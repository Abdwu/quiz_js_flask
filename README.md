# Projet Quiz - Documentation

Ce projet consiste en un système de gestion de quiz où les utilisateurs peuvent consulter, créer, modifier et supprimer des quiz et des questions associées.

## Commandes pour Lancer le Serveur et le Client

1. **Lancer le Serveur Flask :**
   ```bash
   flask run
2. **Créer un quiz**

   Voici la commande pour créer un quiz avec la console 

    ```js 

    create_quiz({name:'nomDuQuiz'})

3. **Créer une question pour un quiz**

   Voici la commande pour créer une question 

   ```js

   create_question({title:"Le nom du Quiz",id: 'id du Quiz'})

4. **Modifier un Quiz**

   Voici la commande pour modifier un Quiz
   
   ```js

   modifier_quiz({name:"Nouveau nom",id:'id du Quiz'})

5. **Modifier une question**

   Voici la commande pour modifier une question

   ```js

   modifier_question({title:"Nouveai nom",id: 'id de la question'})

6. **Suppresion**

   Commande pour supprimer un quiz ou une question
   

   ```js
   // Un Quiz
   delete_quiz({id: 'id du Quiz'})

   // Une Question
   delete_question({id: 'id de la question'})

### Remarque : l'id ne doit pas être dans des paranthèse



## Fonctionalité implémenter : 

1. Quiz :
- Créer 
- Supprimer
- Modifier

2. Question : 
- Créer 
- Supprimer
- Modifier

## Les Curl

Dans le fichier curl.txt